//SOUNDS
var audio = [];

//TIMEOUTS
var timeout = [],
	timer,timer2,
	launch = [],
	globalName,
	sprite = [],
	typingText = [];

var firstTime108 = true,
	hundRule = true;
	
//AWARDs NUMBER
var awardNum = 0;

var presentPlane = function(){
	var plane = $(".plane-center"),
		planeNum = $(".award-box .award-num");
	
	audio[0] = new Audio("audio/s17-1.mp3"),
			
	awardNum ++;
	
	audio[0].play();
	
	timeout[0] = setTimeout(function(){
		plane.fadeIn(1000);
	}, 2000);
	timeout[1] = setTimeout(function(){
		plane.addClass("plane-corner");
	}, 3000);
	timeout[2] = setTimeout(function(){
		plane.fadeOut(0);
		plane.removeClass("plane-corner");
		planeNum.html(awardNum);
	}, 5000);
}

var plusOneSec = function(audio)
{
	var currTime = Math.round(audio.currentTime);
	audio.currentTime = ++currTime;
}

var startTimer = function(jqueryElement, secondsLeft, callBack)
{
	if(secondsLeft <= 0)
		callBack();
	else
	{
		secondsLeft --;
		var minutesLeft = Math.floor(secondsLeft / 60);
		var result = "";
		secondsLeft = Math.round(secondsLeft % 60);
		
		if(minutesLeft > 9) 
			result = "" + minutesLeft + ":";
		else
			result = "0" + minutesLeft + ":";
		
		if(secondsLeft > 9)
			result += secondsLeft;
		else 
			result += "0" + secondsLeft;
		
		jqueryElement.html(result);
		
		secondsLeft += (minutesLeft * 60);
		
		timer = setTimeout(function(){
			startTimer(jqueryElement, secondsLeft, callBack);
		}, 1000);
	}
}

var startTimer2 = function(jqueryElement, secondsLeft)
{
	secondsLeft ++;
	var minutesLeft = Math.floor(secondsLeft / 60);
	var result = "";
	secondsLeft = Math.round(secondsLeft % 60);
	
	if(minutesLeft === 0)
		jqueryElement.css("color", "green");
	else
		jqueryElement.css("color", "black");
	
	if(minutesLeft > 9) 
		result = "" + minutesLeft + ":";
	else
		result = "0" + minutesLeft + ":";
	
	if(secondsLeft > 9)
		result += secondsLeft;
	else 
		result += "0" + secondsLeft;
	
	jqueryElement.html(result);
	
	secondsLeft += (minutesLeft * 60);
	
	timer2 = setTimeout(function(){
		startTimer2(jqueryElement, secondsLeft);
	}, 1000);
}

var stopTimer = function()
{
	clearTimeout(timer);
}

var stopTimer2 = function()
{
	clearTimeout(timer2);
}

var posAndSize = function(left, top, width, height)
{
	var pageW = 2000,
		pageH = 1000,
		leftP = left / 2000 * 100, 
		topP = top / 1000 * 100, 
		widthP = width / 2000 * 100,
		heightP = height / 1000 * 100;
		
	var css = {
		"left": leftP + "%",
		"top": topP + "%",
		"width": widthP + "%",
		"height": heightP + "%"
	};
	
	return css;
}
	
var allHaveHtml = function(jqueryElement){
	for(var i = 0; i < jqueryElement.length; i ++)
	{
		if(!$(jqueryElement[i]).html())
			return false;
	}
	return true;
}

var fadeOneByOne = function(jqueryElement, curr, interval, endFunction)
{
	if (curr < jqueryElement.length)
	{
		timeout[curr] = setTimeout(function(){
			$(jqueryElement[curr]).fadeIn(200);
			fadeOneByOne(jqueryElement, ++curr, interval, endFunction);
		}, interval);
	}
	else
		endFunction();
}

var fadeOneByOne2 = function(jqueryElement, curr, interval, endFunction) //THE PREV ELEMENT FADES OUT
{
	if (curr < jqueryElement.length)
	{
		timeout[curr] = setTimeout(function(){
			$(jqueryElement[curr]).fadeIn(200);
			if(curr > 0)
				$(jqueryElement[curr - 1]).fadeOut(0);
			fadeOneByOne2(jqueryElement, ++curr, interval, endFunction);
		}, interval);
	}
	else
		endFunction();
}

function TypingText(jqueryElement, interval, hasSound, endFunction)
{
	this.text = jqueryElement.html();
	this.jqueryElement = jqueryElement;
	this.interval = interval;
	this.currentSymbol = 0;
	this.endFunction = endFunction;
	this.hasSound = hasSound;
	this.audio = new Audio("audio/keyboard-sound.mp3");
	this.audio.addEventListener("ended", function(){
			this.play();
		});
	
	this.timeout;
	this.jqueryElement.html("");
}

TypingText.prototype.write = function()
{
	tObj = this;
	if (tObj.audio.paused && tObj.hasSound) {
		tObj.audio.play();
	}
	tObj.timeout = setTimeout(function(){
		if (tObj.text[tObj.currentSymbol] === "/" && tObj.text[tObj.currentSymbol + 1] === 'n')
		{
			tObj.jqueryElement.append("<br>");
			tObj.currentSymbol += 2;
		}
		else if (tObj.text[tObj.currentSymbol] === "/" && tObj.text[tObj.currentSymbol + 1] === 's')
		{
			tObj.currentSymbol += 2;
			tObj.jqueryElement.append("<sup>" + tObj.text[tObj.currentSymbol] + "</sup>");
			tObj.currentSymbol ++;
		}
		else if (tObj.text[tObj.currentSymbol] === "/" && tObj.text[tObj.currentSymbol + 1] === 'b')
		{
			tObj.currentSymbol += 2;
			tObj.jqueryElement.append("<b>" + tObj.text[tObj.currentSymbol] + "</b>");
			tObj.currentSymbol ++;
		}
		else
		{
			tObj.jqueryElement.append(tObj.text[tObj.currentSymbol]);
			tObj.currentSymbol ++;
		}
		if (tObj.currentSymbol < tObj.text.length) 
			tObj.write();
		else
		{
			tObj.audio.pause();
			tObj.endFunction();
		}
	}, tObj.interval);
}

TypingText.prototype.stopWriting = function()
{
	clearTimeout(this.timeout);
	this.audio.pause();
}

function DragTask(jqueryElements, successCondition, successFunction, failFunction, finishCondition, finishFunction)
{
	this.draggables = jqueryElements;
	this.draggabillies = [];
	this.vegetable;
	this.basket;
	
	this.makeThemDraggable = function()
	{
		for(var i = 0; i < this.draggables.length; i++)
			this.draggabillies[i] = new Draggabilly(this.draggables[i]);
	}
	
	this.addEventListeners = function()
	{
		for(var i = 0; i < this.draggabillies.length; i++)
		{
			this.draggabillies[i].on("dragStart", this.onStart);
			this.draggabillies[i].on("dragEnd", this.onEnd);
		}
	}
	
	this.onEnd = function(instance, event, pointer)
	{
		var currVeg = this.vegetable;
		var currBasket = this.basket;
		currVeg.fadeOut(0);
		currBasket = $(document.elementFromPoint(pointer.pageX, pointer.pageY));
		currVeg.fadeIn(0);
		
		if (currBasket.attr("data-key") && successCondition(currVeg.attr("data-key"), currBasket.attr("data-key")))
		{
			successFunction(currVeg, currBasket);
			if (finishCondition())
			{
				finishFunction();
			}
		}
		else
			failFunction(currVeg, currBasket);
		
		currVeg.removeClass("box-shadow-white");
		currVeg.css("opacity", "");
	}
	
	this.onStart = function(instance, event, pointer)
	{
		this.vegetable = $(event.target);
		this.vegetable.css("z-index", "9999");
		this.vegetable.addClass("box-shadow-white");
		this.vegetable.css("opacity", "0.6");
	}
	
	this.makeThemDraggable();
	this.addEventListeners();
}

var blink = function(jqueryElements, color, interval, times, callBack)
{
	var intervalHalf = Math.round(interval/2);
	timeout[0] = setTimeout(function(){
		jqueryElements.css("background-color", color);
		timeout[1] = setTimeout(function(){
			jqueryElements.css("background-color", "");
			if (times) 
				blink(jqueryElements, color, interval, --times, callBack)		
			else
				callBack();
		}, intervalHalf);
	}, intervalHalf);
}

var blink2 = function(jqueryElements, interval, times, callBack)
{
	var intervalHalf = Math.round(interval/2);
	timeout[0] = setTimeout(function(){
		jqueryElements.addClass("blink-shadow");
		timeout[1] = setTimeout(function(){
			jqueryElements.removeClass("blink-shadow");
			if (times) 
				blink2(jqueryElements, interval, --times, callBack)		
			else
				callBack();
		}, intervalHalf);
	}, intervalHalf);
}

var isCorrect = function(jqueryElement)
{
	var val1, val2;
	
	if(jqueryElement.html() == "")
		val1 = 1;
	else
		val1 = parseInt(jqueryElement.html());
	
	val2 = parseInt(jqueryElement.attr("data-correct"));
	
	return val1 === val2;
}

var setLRSData = function(){
	tincan = new TinCan (
    {
        recordStores: [
            {
                "endpoint": "http://54.154.57.220/data/xAPI/",
				"username": "e083498f4e256e68ab2c5ae2be4195d9a348eb20",
				"password": "c6ea3c32a9d77919d0eb3cdea3bc5d460f50d93b"
            }
        ]
    });
}

var sendLaunchedStatement = function(sm)
{
	if(globalName)
	{
		tincan.sendStatement(
		{
			"actor": {
				"objectType": "Agent",
				"account": {
					"name": globalName,
					"homePage": "http://lcms.nis.kz"
				}
			},
			"verb": {
				"id": "http://adlnet.gov/expapi/verbs/launched"
			},
			"context": {
				"platform": "web"
			},
			"object": {
				"id": "http://lcms.nis.kz/activity/821eb006-58bb-4154-b732-4b2a9a9330ad",
				"objectType": "Activity",
				"description": "frame-" + sm
			}
		});
	}
}

var sendCompletedStatement = function(sm)
{
	if(globalName)
	{
		tincan.sendStatement(
		{
			"actor": {
				"objectType": "Agent",
				"account": {
					"name": globalName,
					"homePage": "http://lcms.nis.kz/chemistry"
				}
			},
			"verb": {
				"id": "http://adlnet.gov/expapi/verbs/completed"
			},
			"context": {
				"platform": "web"
			},
			"object": {
				"id": "http://lcms.nis.kz/activity/821eb006-58bb-4154-b732-4b2a9a9330ad",
				"objectType": "Activity",
				"description": "frame-" + sm
			}
		});
	}
}

var setMenuStuff = function(){
	var enterButton = $(".enter-button"),
		annotation = $("#frame-000 .annotation-hidden"),
		annotationButton = $("#frame-000 .annotation-button"),
		password = $(".password"),
		error = $(".error"),
		name = $(".name");
		name.val("");
		password.val("");
		
		
		sayHello = function(){
		regBox.html("Здравствуйте, " + name.val() + "!");
		regBox.css("height", "3em");
		regBox.css("padding", "0.5em");
		regBox.css("text-align", "center");
		globalName = name.val();
		regBox.addClass("topcorner");
		timeout[0] = setTimeout(function(){
			regBox.html(name.val());
			regBox.css("width", name.val().length + "em");
		}, 3000);
	};
	
	if(globalName)
		sayHello();
	
	var annotationButtonListener = function(){
		annotation.toggleClass("annotation-shown");
		annotationButton.toggleClass("annotation-button-close");
	};
	annotationButton.off("click", annotationButtonListener);
	annotationButton.on("click", annotationButtonListener);
	
	var enterButtonListener = function(){
		if(password.val() === "12345")
			sayHello();
		else
			error.html("неверный пароль");
	};
	enterButton.off("click", enterButtonListener);
	enterButton.on("click", enterButtonListener);
}

var meetsHund = function(baskets)
{
	for (var i = 0; i < baskets.length; i++)
	{
		if($(baskets[i]).attr("data-key") !== "nan" && $(baskets[i]).attr("data-key") !== "done")
			return false;
	}
	
	return true;
}

launch["frame-000"] = function() {
	
}

launch["frame-101"] = function() {
		theFrame = $("#frame-101"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		bg1 = $(prefix + ".bg-1"),
		bg2 = $(prefix + ".bg-2"),
		bg3 = $(prefix + ".bg-3");
	
	audio[0] = new Audio("audio/s1-1.mp3");
		
	bg2.fadeOut(0);
	bg3.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	
	startButtonListener = function(){
		timeout[1] = setTimeout(function(){
			bg2.fadeIn(1000);
		}, 1000);
		timeout[2] = setTimeout(function(){
			bg3.fadeIn(1000);
		}, 5000);
		timeout[3] = setTimeout(function(){
			audio[0].play();
		}, 6000);
		timeout[4] = setTimeout(function(){
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
			sendCompletedStatement(1);
			audio[0].pause();
		}, 33500);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(1);
	}, 500);
}

launch["frame-102"] = function() {
		theFrame = $("#frame-102"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		bor = $(prefix + '.bor'),
		proton = $(prefix + '.proton'),
		neutron = $(prefix + '.neutron'),
		formula = $(prefix + '.formula'),
		formulaTwo = $(prefix + '.formula-two');
		numberTwo = $(prefix + '.number-two'),
		numberTwoLight = $(prefix + '.number-two-light'),
		numberThree = $(prefix + '.number-three'),
		numberThreeLight = $(prefix + '.number-three-light'),
		numberFive = $(prefix + '.number-five'),
		numberFiveLight = $(prefix + '.number-five-light'),
		nucleus = $(prefix + '.nucleus'),
		levels = $(prefix + '.levels'),
		twoElectrons = $(prefix + '.two-electrons'),
		threeElectrons =  $(prefix + '.three-electrons') ;

	formula.fadeOut(0);
	formulaTwo.fadeOut(0);
	nucleus.fadeOut(0);	
	numberTwoLight.fadeOut(0);
	numberThreeLight.fadeOut(0);
	numberFiveLight.fadeOut(0);
	twoElectrons.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	
	
	bor.css("left", "32%");

	audio[0] = new Audio("audio/s1-1.mp3");	
	
	sprite[0] = new Motio(proton[0], {
		"fps": "3", 
		"frames": "12"
	});

	sprite[0].on("frame", function(){
		if(this.frame === this.frames - 1)
		{
			this.pause();
			setTimeout(function() {
				numberTwoLight.fadeIn(0);
				numberTwo.fadeOut(0);	
			},500);
		}
	});

	sprite[1] = new Motio(neutron[0], {
		"fps": "3", 
		"frames": "11"
	});
	
	sprite[1].on("frame", function(){
		if(this.frame === this.frames - 1)
		{
			this.pause();
			nucleus.fadeIn(0);
		}
	});

	sprite[2] = new Motio(levels[0], {
		"fps": "3", 
		"frames": "12"
	});

	sprite[2].on("frame", function(){
		if(this.frame === this.frames - 1)
		{
			this.pause();			
		}
	});

	sprite[3] = new Motio(threeElectrons[0], {
		"fps": "3", 
		"frames": "9"
	});

	sprite[3].on("frame", function(){
		if(this.frame === this.frames - 1)
		{
			this.pause();			
		}
	});

	neutron.hide();
	levels.hide();
	threeElectrons.hide();
	proton.hide();
	numberThree.fadeOut(0);
	numberFive.fadeOut(0);
	numberTwo.fadeOut(0);

	audio[0].addEventListener("ended", function() {
		setTimeout(function() {
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
			sendCompletedStatement(2);
		},3000)
	});

	startButtonListener = function(){
		bor.addClass("box-shadow-yellow-big");
		
		audio[0].currentTime = 30.5;
		audio[0].play();
		
		timeout[0] = setTimeout(function(){
			bor.addClass("transition-2s");
			bor.css("left", "");
		}, 4000);
		timeout[1] = setTimeout(function(){
			numberFive.fadeIn(0);
		}, 7000);
		timeout[2] = setTimeout(function(){
			numberFiveLight.fadeIn(0);
			numberFive.fadeOut(0);
		}, 8000);
		timeout[3] = setTimeout(function(){
			numberFiveLight.fadeOut(0);
			numberFive.fadeIn(0);
		}, 14000);
		timeout[4] = setTimeout(function(){
			proton.show();
			sprite[0].play();
			numberFiveLight.fadeOut(0);
			numberFive.fadeIn(0);
		}, 15000);
		timeout[5] = setTimeout(function(){
			formula.fadeIn(0);
			formulaTwo.fadeIn(0);
		}, 17000);
		timeout[6] = setTimeout(function(){
			neutron.show();
			sprite[1].play();
		}, 19000);
		timeout[7] = setTimeout(function(){
			numberTwo.fadeIn(0);
		}, 20000);
		timeout[8] = setTimeout(function(){
			numberTwoLight.fadeIn(0);
		}, 21000);
		timeout[9] = setTimeout(function(){
			numberTwoLight.fadeOut(0);
		}, 23000);
		timeout[10] = setTimeout(function(){
			levels.fadeIn(0);
		}, 24000);
		timeout[11] = setTimeout(function(){
			sprite[2].play();
		}, 25000);
		timeout[12] = setTimeout(function(){
			numberThree.fadeIn(0);
		}, 29000);
		timeout[13] = setTimeout(function(){
			threeElectrons.fadeIn(0);
		}, 30000);
		timeout[14] = setTimeout(function(){
			sprite[3].play();
		}, 31000);
	};
	
	startButtonListener();
	sendLaunchedStatement(2);
}

launch["frame-103"] = function() {
		theFrame = $("#frame-103"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		bg1 = $(prefix + ".bg-1"),
		bg2 = $(prefix + ".bg-2"),
		cloud = $(prefix + ".cloud");
	
	audio[0] = new Audio("audio/s2-1.mp3"), 
	audio[1] = new Audio("audio/s3-1.mp3");
	
	audio[0].addEventListener("ended", function(){
		timeout[0] = setTimeout(function(){
			audio[1].play();
		}, 1000);
	});
	audio[1].addEventListener("ended", function(){
		theFrame.attr("data-done", "true");
		hideEverythingBut($("#frame-105"));
		sendCompletedStatement(1);
	});
	
	//cloud.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	
	startButtonListener = function(){
		timeout[1] = setTimeout(function(){
			//cloud.fadeIn(1000);
			audio[0].play();
		}, 1000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(3);
	}, 1000);
}

launch["frame-105"] = function() {
		theFrame = $("#frame-105"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		bg1 = $(prefix + ".bg-1"),
		bg2 = $(prefix + ".bg-2"),
		bg3 = $(prefix + ".bg-3"), 
		bg4 = $(prefix + ".bg-4"), 
		bulb = $(prefix + ".bulb"),
		scheme = $(prefix + ".scheme");
	
	schemeSprite = new Motio(scheme[0], {
		"frames": "6",
		"fps": "0.3"
	});
	schemeSprite.on("frame", function(){
		if(this.frame === this.frames - 1)
		{
			this.pause();
			timeout[5] = setTimeout(function(){
				theFrame.attr("data-done", "true");
				fadeNavsInAuto();
				sendCompletedStatement(1);
			}, 3000);
		}
	});
	
	audio[0] = new Audio("audio/ding-sound.mp3");
	audio[1] = new Audio("audio/s4-1.mp3");
	
	bg3.fadeOut(0);
	bg4.fadeOut(0);
	bulb.fadeOut(0);
	scheme.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			audio[0].play();
			bg2.fadeOut(0);
			bg3.fadeIn(0);
			bulb.fadeIn(0);
		}, 1000);
		timeout[2] = setTimeout(function(){
			audio[1].play();
		}, 2000);
		timeout[3] = setTimeout(function(){
			bg4.fadeIn(500);
			scheme.fadeIn(500);
		}, 3000);
		timeout[4] = setTimeout(function(){
			schemeSprite.play();
		}, 5000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(5);
	}, 500);
}

launch["frame-106"] = function() {
		theFrame = $("#frame-106"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		bg = $(prefix + ".bg"),
		els = $(prefix + ".el");
	
	audio[0] = new Audio("audio/s5-1.mp3");
	
	fadeNavsOut();
	fadeLauncherIn();
	els.fadeOut(0);
	
	startButtonListener = function(){
		audio[0].play();
		fadeOneByOne2(els, 0, 2000, function(){
			timeout[0] = setTimeout(function(){
				theFrame.attr("data-done", "true");
				fadeNavsInAuto();
				sendCompletedStatement(6);
			}, 2000);
		});
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(6);
	}, 2000);
}

launch["frame-107"] = function() {
		theFrame = $("#frame-107"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		bg = $(prefix + ".bg"),
		anim = $(prefix + ".anim"),
		filling = $(prefix + ".filling"),
		rollButton = $(prefix + ".roll-button"),
		roll = $(prefix + ".roll");
	
	audio[0] = new Audio("audio/s6-1.mp3");
	
	var rollButtonListener = function(){
		roll.fadeToggle(500);
	};
	rollButton.off("click", rollButtonListener);
	rollButton.on("click", rollButtonListener);
	
	sprite[0] = new Motio(anim[0], {
		"frames": "10", 
		"fps": "1"
	});
	
	anim.fadeOut(0);
	filling.fadeOut(0);
	rollButton.fadeOut(0);
	roll.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	
	startButtonListener = function(){
		audio[0].play();
			
		timeout[1] = setTimeout(function(){
			anim.fadeIn(0);
		}, 4500);
		
		timeout[2] = setTimeout(function(){
			sprite[0].to(1, true);
		}, 7000);
		
		timeout[3] = setTimeout(function(){
			sprite[0].to(2, true);
		}, 10000);
		
		timeout[4] = setTimeout(function(){
			sprite[0].to(3, true);
			sprite[0].play();
		}, 21000);
		
		timeout[5] = setTimeout(function(){
			sprite[0].pause();
		}, 25000);
		
		timeout[6] = setTimeout(function(){
			sprite[0].to(7, true);
		}, 26000);
		
		timeout[7] = setTimeout(function(){
			sprite[0].to(8, true);
		}, 27000);
		
		timeout[8] = setTimeout(function(){
			sprite[0].to(9, true);
		}, 32000);
		
		timeout[9] = setTimeout(function(){
			anim.fadeOut(0);
			filling.fadeIn(0);
			rollButton.fadeIn(1000);
		}, 33000);
		
		timeout[10] = setTimeout(function(){
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
			sendCompletedStatement(7);
		}, 45000);
	};	
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(7);
	}, 2000);
}

launch["frame-108"] = function() {
		theFrame = $("#frame-108"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		keepThinking = $(prefix + ".keep-thinking"),
		elems = $(prefix + ".elem");
	
	audio[0] = new Audio("audio/s7-1.mp3");
	
	audio[0].addEventListener("ended", function(){
		keepThinking.fadeIn(500);
		timeout[0] = setTimeout(function(){
			keepThinking.fadeOut(500);
		}, 3000);
	});
	
	fadeNavsOut();
	fadeLauncherIn();
	keepThinking.fadeOut(0);

	timeout[0] = setTimeout(function(){
		sendLaunchedStatement(8);
		if(firstTime108)
		{
			firstTime108 = false;
			audio[0].play();
		}
		else
		{
			initMenuButtons();
		}
	}, 1000);
}

launch["frame-109"] = function() {
		theFrame = $("#frame-109"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		newElementButton = $(prefix + ".new-element-button"),
		ballContainer = $(prefix + ".ball-container"),
		bgs = $(prefix + ".bg"),
		bgBase = $(prefix + ".bg-base"), 
		ball = $(prefix + ".ball"),
		baskets = $(prefix + ".basket"),
		electrons = $(prefix + ".electron"),
		trueFalse = $(prefix + ".true-false"),
		s1 = $(prefix + ".s1"),
		s2 = $(prefix + ".s2"),
		s3 = $(prefix + ".s3"), 
		s4 = $(prefix + ".s4"),
		p2 = $(prefix + ".p2"),
		p3 = $(prefix + ".p3"),
		activeLevel = 1,
		activeBasket = 1;
	
	audio[0] = new Audio("audio/s9-1.mp3");
	audio[1] = new Audio("audio/s11-1.mp3");
	
	s1Sprite = new Motio(s1[0], {
		"frames": "2"
	});
	s2Sprite = new Motio(s2[0], {
		"frames": "2"
	});
	s3Sprite = new Motio(s3[0], {
		"frames": "2"
	});
	s4Sprite = new Motio(s4[0], {
		"frames": "2"
	});
	p2Sprite = new Motio(p2[0], {
		"frames": "6"
	});
	p3Sprite = new Motio(p3[0], {
		"frames": "6"
	});
	
	var successCondition = function(vegetable, basket){
		return vegetable === basket;
	};
	var successFunction = function(vegetable, basket){
		vegetable.css("left", "");
		vegetable.css("top", "");
		vegetable.css("z-index", "");
		var veg = vegetable.clone();
		basket.append(veg);
		
		if(basket.attr("data-key") === "up" && basket.attr("data-uponly"))
			basket.attr("data-key", "done");
		else if(basket.attr("data-key") === "up")
			basket.attr("data-key", "nan");
		else if(basket.attr("data-key") === "down")
			basket.attr("data-key", "done");
		
		activeBasket++;			
	
		var nextBasket = $(prefix + ".level-" + activeLevel + ".basket-" + activeBasket);
		
		if(nextBasket.length)
		{
			if(nextBasket.attr("data-key") === "initial")
			{
				nextBasket.attr("data-key", "up");
			}
			else if(nextBasket && nextBasket.attr("data-key") === "nan")
			{
				nextBasket.attr("data-key", "down");
			}
		}
		else 
		{
			if($(prefix + ".level-" + activeLevel + ".basket-1").attr("data-key") === "nan")
			{
				$(prefix + ".level-" + activeLevel + ".basket-1").attr("data-key", "down");
				activeBasket = 1;
			}				
			else
			{
				activeLevel ++;
				activeBasket = 1;
				if($(prefix + ".level-" + activeLevel + ".basket-" + activeBasket).attr("data-key") !== "done")
					$(prefix + ".level-" + activeLevel + ".basket-" + activeBasket).attr("data-key", "up");
			}
		}
		
		var clName = basket.attr("data-electron");
		switch(clName)
		{
			case "s1": s1.css("display") === "none" ? s1.fadeIn(0) : s1Sprite.to(s1Sprite.frame + 1, true); break;
			case "s2": s2.css("display") === "none" ? s2.fadeIn(0) : s2Sprite.to(s2Sprite.frame + 1, true); break;
			case "s3": s3.css("display") === "none" ? s3.fadeIn(0) : s3Sprite.to(s3Sprite.frame + 1, true); break;
			case "s4": s4.css("display") === "none" ? s4.fadeIn(0) : s4Sprite.to(s4Sprite.frame + 1, true); break;
			case "p2": p2.css("display") === "none" ? p2.fadeIn(0) : p2Sprite.to(p2Sprite.frame + 1, true); break;
			case "p3": p3.css("display") === "none" ? p3.fadeIn(0) : p3Sprite.to(p3Sprite.frame + 1, true); break;
		}
		
		if(basket.attr("data-key") === "done")
			basket.append("<div class='point'></div>");
	};
	var failFunction = function(vegetable, basket){
		vegetable.css("left", "");
		vegetable.css("top", "");
		vegetable.css("z-index", "");
	};
	var finishCondition = function(){
		for(var i = 0; i < baskets.length; i++)
			if ($(baskets[i]).attr("data-key") !== "done")
				return false;
		return true;
	};
	var finishFunction = function(){
		newElementButton.fadeIn(500);
		presentPlane();
	};
	
	var dragTask;
	
	electrons.fadeOut(0);
	newElementButton.fadeOut(0);
	trueFalse.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	
	var explainHund = function(){
		audio[0].play();
		blink2(ballContainer, 1000, 3, function(){
			blink2(baskets, 1000, 3, function(){
				timeout[1] = setTimeout(function(){
					trueFalse.fadeIn(500);
					timeout[2] = setTimeout(function(){
						trueFalse.fadeOut(500);
						hundRule = false;
						dragTask = new DragTask(ball, successCondition, successFunction, failFunction, finishCondition, finishFunction);
					}, 6000);
				}, 3000);	
			});
		});
	}
	
	if(hundRule)
	{
		timeout[0] = setTimeout(explainHund, 2000);
	}
	else
	{
		dragTask = new DragTask(ball, successCondition, successFunction, failFunction, finishCondition, finishFunction);
	}
}

launch["frame-110"] = function() {
		theFrame = $("#frame-110"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		newElementButton = $(prefix + ".new-element-button"),
		ballContainer = $(prefix + ".ball-container"),
		bgs = $(prefix + ".bg"),
		bgBase = $(prefix + ".bg-base"), 
		ball = $(prefix + ".ball"),
		baskets = $(prefix + ".basket"),
		electrons = $(prefix + ".electron"),
		trueFalse = $(prefix + ".true-false"),
		s1 = $(prefix + ".s1"),
		s2 = $(prefix + ".s2"),
		activeLevel = 1,
		activeBasket = 1;
	
	audio[0] = new Audio("audio/s9-1.mp3");
	audio[1] = new Audio("audio/s11-1.mp3");
	
	s1Sprite = new Motio(s1[0], {
		"frames": "2"
	});
	s2Sprite = new Motio(s2[0], {
		"frames": "2"
	});
	
	var successCondition = function(vegetable, basket){
		return vegetable === basket;
	};
	var successFunction = function(vegetable, basket){
		vegetable.css("left", "");
		vegetable.css("top", "");
		vegetable.css("z-index", "");
		var veg = vegetable.clone();
		basket.append(veg);
		
		if(basket.attr("data-key") === "up" && basket.attr("data-uponly") === "true")
			basket.attr("data-key", "done");
		else if(basket.attr("data-key") === "up")
			basket.attr("data-key", "nan");
		else if(basket.attr("data-key") === "down")
			basket.attr("data-key", "done");
		
		activeBasket++;			
	
		var nextBasket = $(prefix + ".level-" + activeLevel + ".basket-" + activeBasket);
		
		if(nextBasket.length)
		{
			if(nextBasket.attr("data-key") === "initial")
			{
				nextBasket.attr("data-key", "up");
			}
			else if(nextBasket && nextBasket.attr("data-key") === "nan")
			{
				nextBasket.attr("data-key", "down");
			}
		}
		else 
		{
			if($(prefix + ".level-" + activeLevel + ".basket-1").attr("data-key") === "nan")
			{
				$(prefix + ".level-" + activeLevel + ".basket-1").attr("data-key", "down");
				activeBasket = 1;
			}				
			else
			{
				activeLevel ++;
				activeBasket = 1;
				if($(prefix + ".level-" + activeLevel + ".basket-" + activeBasket).attr("data-key") !== "done")
					$(prefix + ".level-" + activeLevel + ".basket-" + activeBasket).attr("data-key", "up");
			}
		}
		
		var clName = basket.attr("data-electron");
		switch(clName)
		{
			case "s1": s1.css("display") === "none" ? s1.fadeIn(0) : s1Sprite.to(s1Sprite.frame + 1, true); break;
			case "s2": s2.css("display") === "none" ? s2.fadeIn(0) : s2Sprite.to(s2Sprite.frame + 1, true); break;
			case "s3": s3.css("display") === "none" ? s3.fadeIn(0) : s3Sprite.to(s3Sprite.frame + 1, true); break;
			case "s4": s4.css("display") === "none" ? s4.fadeIn(0) : s4Sprite.to(s4Sprite.frame + 1, true); break;
			case "p2": p2.css("display") === "none" ? p2.fadeIn(0) : p2Sprite.to(p2Sprite.frame + 1, true); break;
			case "p3": p3.css("display") === "none" ? p3.fadeIn(0) : p3Sprite.to(p3Sprite.frame + 1, true); break;
		}
		
		if(basket.attr("data-key") === "done")
			basket.append("<div class='point'></div>");
	};
	var failFunction = function(vegetable, basket){
		vegetable.css("left", "");
		vegetable.css("top", "");
		vegetable.css("z-index", "");
	};
	var finishCondition = function(){
		for(var i = 0; i < baskets.length; i++)
			if ($(baskets[i]).attr("data-key") !== "done")
				return false;
		return true;
	};
	var finishFunction = function(){
		newElementButton.fadeIn(500);
		presentPlane();
	};
	
	var dragTask;
	
	electrons.fadeOut(0);
	newElementButton.fadeOut(0);
	trueFalse.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	
	var explainHund = function(){
		audio[0].play();
		blink2(ballContainer, 1000, 3, function(){
			blink2(baskets, 1000, 3, function(){
				timeout[1] = setTimeout(function(){
					trueFalse.fadeIn(500);
					timeout[2] = setTimeout(function(){
						trueFalse.fadeOut(500);
						hundRule = false;
						dragTask = new DragTask(ball, successCondition, successFunction, failFunction, finishCondition, finishFunction);
					}, 6000);
				}, 3000);	
			});
		});
	}
	
	if(hundRule)
	{
		timeout[0] = setTimeout(explainHund, 2000);
	}
	else
	{
		dragTask = new DragTask(ball, successCondition, successFunction, failFunction, finishCondition, finishFunction);
	}
}

launch["frame-111"] = function() {
		theFrame = $("#frame-111"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		newElementButton = $(prefix + ".new-element-button"),
		ballContainer = $(prefix + ".ball-container"),
		bgs = $(prefix + ".bg"),
		bgBase = $(prefix + ".bg-base"), 
		ball = $(prefix + ".ball"),
		baskets = $(prefix + ".basket"),
		electrons = $(prefix + ".electron"),
		trueFalse = $(prefix + ".true-false"),
		s1 = $(prefix + ".s1"),
		s2 = $(prefix + ".s2"),
		p2 = $(prefix + ".p2"),
		activeLevel = 1,
		activeBasket = 1;
	
	audio[0] = new Audio("audio/s9-1.mp3");
	audio[1] = new Audio("audio/s11-1.mp3");
	
	s1Sprite = new Motio(s1[0], {
		"frames": "2"
	});
	s2Sprite = new Motio(s2[0], {
		"frames": "2"
	});
	p2Sprite = new Motio(p2[0], {
		"frames": "6"
	});
	
	var successCondition = function(vegetable, basket){
		return vegetable === basket;
	};
	var successFunction = function(vegetable, basket){
		vegetable.css("left", "");
		vegetable.css("top", "");
		vegetable.css("z-index", "");
		var veg = vegetable.clone();
		basket.append(veg);
		
		if(basket.attr("data-key") === "up" && basket.attr("data-uponly") === "true")
			basket.attr("data-key", "done");
		else if(basket.attr("data-key") === "up")
			basket.attr("data-key", "nan");
		else if(basket.attr("data-key") === "down")
			basket.attr("data-key", "done");
		
		activeBasket++;			
	
		var nextBasket = $(prefix + ".level-" + activeLevel + ".basket-" + activeBasket);
		
		if(nextBasket.length)
		{
			if(nextBasket.attr("data-key") === "initial")
			{
				nextBasket.attr("data-key", "up");
			}
			else if(nextBasket && nextBasket.attr("data-key") === "nan")
			{
				nextBasket.attr("data-key", "down");
			}
		}
		else 
		{
			if($(prefix + ".level-" + activeLevel + ".basket-1").attr("data-key") === "nan")
			{
				$(prefix + ".level-" + activeLevel + ".basket-1").attr("data-key", "down");
				activeBasket = 1;
			}				
			else
			{
				activeLevel ++;
				activeBasket = 1;
				$(prefix + ".level-" + activeLevel + ".basket-" + activeBasket).attr("data-key", "up");
			}
		}
		
		var clName = basket.attr("data-electron");
		switch(clName)
		{
			case "s1": s1.css("display") === "none" ? s1.fadeIn(0) : s1Sprite.to(s1Sprite.frame + 1, true); break;
			case "s2": s2.css("display") === "none" ? s2.fadeIn(0) : s2Sprite.to(s2Sprite.frame + 1, true); break;
			case "s3": s3.css("display") === "none" ? s3.fadeIn(0) : s3Sprite.to(s3Sprite.frame + 1, true); break;
			case "s4": s4.css("display") === "none" ? s4.fadeIn(0) : s4Sprite.to(s4Sprite.frame + 1, true); break;
			case "p2": p2.css("display") === "none" ? p2.fadeIn(0) : p2Sprite.to(p2Sprite.frame + 1, true); break;
			case "p3": p3.css("display") === "none" ? p3.fadeIn(0) : p3Sprite.to(p3Sprite.frame + 1, true); break;
		}
		
		if(basket.attr("data-key") === "done")
			basket.append("<div class='point'></div>");
	};
	var failFunction = function(vegetable, basket){
		vegetable.css("left", "");
		vegetable.css("top", "");
		vegetable.css("z-index", "");
	};
	var finishCondition = function(){
		for(var i = 0; i < baskets.length; i++)
			if ($(baskets[i]).attr("data-key") !== "done")
				return false;
		return true;
	};
	var finishFunction = function(){
		newElementButton.fadeIn(500);
		presentPlane();
	};
	
	var dragTask;
	
	electrons.fadeOut(0);
	newElementButton.fadeOut(0);
	trueFalse.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	
	var explainHund = function(){
		audio[0].play();
		blink2(ballContainer, 1000, 3, function(){
			blink2(baskets, 1000, 3, function(){
				timeout[1] = setTimeout(function(){
					trueFalse.fadeIn(500);
					timeout[2] = setTimeout(function(){
						trueFalse.fadeOut(500);
						hundRule = false;
						dragTask = new DragTask(ball, successCondition, successFunction, failFunction, finishCondition, finishFunction);
					}, 6000);
				}, 3000);	
			});
		});
	}
	
	if(hundRule)
	{
		timeout[0] = setTimeout(explainHund, 2000);
	}
	else
	{
		dragTask = new DragTask(ball, successCondition, successFunction, failFunction, finishCondition, finishFunction);
	}
}

launch["frame-112"] = function() {
		theFrame = $("#frame-112"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		newElementButton = $(prefix + ".new-element-button"),
		ballContainer = $(prefix + ".ball-container"),
		bgs = $(prefix + ".bg"),
		bgBase = $(prefix + ".bg-base"), 
		ball = $(prefix + ".ball"),
		baskets = $(prefix + ".basket"),
		electrons = $(prefix + ".electron"),
		trueFalse = $(prefix + ".true-false"),
		s1 = $(prefix + ".s1"),
		s2 = $(prefix + ".s2"),
		s3 = $(prefix + ".s3"), 
		s4 = $(prefix + ".s4"),
		p2 = $(prefix + ".p2"),
		p3 = $(prefix + ".p3"),
		activeLevel = 1,
		activeBasket = 1;
	
	audio[0] = new Audio("audio/s9-1.mp3");
	audio[1] = new Audio("audio/s11-1.mp3");
	
	s1Sprite = new Motio(s1[0], {
		"frames": "2"
	});
	s2Sprite = new Motio(s2[0], {
		"frames": "2"
	});
	s3Sprite = new Motio(s3[0], {
		"frames": "2"
	});
	s4Sprite = new Motio(s4[0], {
		"frames": "2"
	});
	p2Sprite = new Motio(p2[0], {
		"frames": "6"
	});
	p3Sprite = new Motio(p3[0], {
		"frames": "6"
	});
	
	var successCondition = function(vegetable, basket){
		return vegetable === basket;
	};
	var successFunction = function(vegetable, basket){
		vegetable.css("left", "");
		vegetable.css("top", "");
		vegetable.css("z-index", "");
		var veg = vegetable.clone();
		basket.append(veg);
		
		if(basket.attr("data-key") === "up" && basket.attr("data-uponly") === "true")
			basket.attr("data-key", "done");
		else if(basket.attr("data-key") === "up")
			basket.attr("data-key", "nan");
		else if(basket.attr("data-key") === "down")
			basket.attr("data-key", "done");
		
		activeBasket++;			
	
		var nextBasket = $(prefix + ".level-" + activeLevel + ".basket-" + activeBasket);
		
		if(nextBasket.length)
		{
			if(nextBasket.attr("data-key") === "initial")
			{
				nextBasket.attr("data-key", "up");
			}
			else if(nextBasket && nextBasket.attr("data-key") === "nan")
			{
				nextBasket.attr("data-key", "down");
			}
		}
		else 
		{
			if($(prefix + ".level-" + activeLevel + ".basket-1").attr("data-key") === "nan")
			{
				$(prefix + ".level-" + activeLevel + ".basket-1").attr("data-key", "down");
				activeBasket = 1;
			}				
			else
			{
				activeLevel ++;
				activeBasket = 1;
				if($(prefix + ".level-" + activeLevel + ".basket-" + activeBasket).attr("data-key") !== "done")
					$(prefix + ".level-" + activeLevel + ".basket-" + activeBasket).attr("data-key", "up");
			}
		}
		
		var clName = basket.attr("data-electron");
		switch(clName)
		{
			case "s1": s1.css("display") === "none" ? s1.fadeIn(0) : s1Sprite.to(s1Sprite.frame + 1, true); break;
			case "s2": s2.css("display") === "none" ? s2.fadeIn(0) : s2Sprite.to(s2Sprite.frame + 1, true); break;
			case "s3": s3.css("display") === "none" ? s3.fadeIn(0) : s3Sprite.to(s3Sprite.frame + 1, true); break;
			case "s4": s4.css("display") === "none" ? s4.fadeIn(0) : s4Sprite.to(s4Sprite.frame + 1, true); break;
			case "p2": p2.css("display") === "none" ? p2.fadeIn(0) : p2Sprite.to(p2Sprite.frame + 1, true); break;
			case "p3": p3.css("display") === "none" ? p3.fadeIn(0) : p3Sprite.to(p3Sprite.frame + 1, true); break;
		}
		
		if(basket.attr("data-key") === "done")
			basket.append("<div class='point'></div>");
	};
	var failFunction = function(vegetable, basket){
		vegetable.css("left", "");
		vegetable.css("top", "");
		vegetable.css("z-index", "");
	};
	var finishCondition = function(){
		for(var i = 0; i < baskets.length; i++)
			if ($(baskets[i]).attr("data-key") !== "done")
				return false;
		return true;
	};
	var finishFunction = function(){
		newElementButton.fadeIn(500);
		presentPlane();
	};
	
	var dragTask;
	
	electrons.fadeOut(0);
	newElementButton.fadeOut(0);
	trueFalse.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	
	var explainHund = function(){
		audio[0].play();
		blink2(ballContainer, 1000, 3, function(){
			blink2(baskets, 1000, 3, function(){
				timeout[1] = setTimeout(function(){
					trueFalse.fadeIn(500);
					timeout[2] = setTimeout(function(){
						trueFalse.fadeOut(500);
						hundRule = false;
						dragTask = new DragTask(ball, successCondition, successFunction, failFunction, finishCondition, finishFunction);
					}, 6000);
				}, 3000);	
			});
		});
	}
	
	if(hundRule)
	{
		timeout[0] = setTimeout(explainHund, 2000);
	}
	else
	{
		dragTask = new DragTask(ball, successCondition, successFunction, failFunction, finishCondition, finishFunction);
	}
}

launch["frame-114"] = function() {
		theFrame = $("#frame-114"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		arrows = $(prefix + ".arrow"),
		buttons = $(prefix + ".button"),
		sketchpadContainer = $(prefix + ".sketchpad-container"),
		sketchpadJQ = $(prefix + "#responsive-sketchpad"),
		keepThinking = $(prefix + ".keep-thinking"), 
		newPicButton = $(prefix + ".new-pic-button"),
		nextFrameButton = $(prefix + ".next-frame-button");
	
	audio[0] = new Audio("audio/s12-1.mp3");
	
	done = [];
	done[5] = false;
	
	audio[0].addEventListener("timeupdate", function(){ 
		currAudio = this;
		currTime = Math.round(currAudio.currentTime);
		if(currTime === 5 && !done[5])
		{
			done[5] = true;
			blink2(sketchpadJQ, 1000, 1, function(){
				blink2(buttons, 1000, 1, function(){
					keepThinking.fadeIn(500);
					timeout[0] = setTimeout(function(){
						keepThinking.fadeOut(500);
					}, 1000);
				});
			});
		}
	});
	
	var sketchpad = sketchpadJQ.sketchpad(
	{
		aspectRatio: 5/3            // (Required) To preserve the drawing, an aspect ratio must be specified
	});	
	
	sketchpad.setLineColor('#177EE5');
	sketchpad.setLineSize(3);
	
	$(".sketch-control").click(function(){
		switch($(this).attr("id"))
		{
			case "clear":
				sketchpad.clear();
				break;
			case"undo":
				sketchpad.undo();
				break;
			case "red":
				sketchpad.setLineColor('#D5000D');
				break;
			case "blue":
				sketchpad.setLineColor('#177EE5');
				break;
			case "download":
				var canvas = document.getElementById("responsive-sketchpad");
				var context = canvas.getContext("2d");
				context.fillStyle = "green";
				window.open(canvas.toDataURL("image/png"), '_blank');
				newPicButton.fadeIn(500);
				timeout[0] = setTimeout(function(){
					theFrame.attr("data-done", "true");
					fadeNavsInAuto();
					sendCompletedStatement(10);
				}, 3000);
				break;
		}
	});
	
	arrows.fadeOut(0);
	keepThinking.fadeOut(0);
	newPicButton.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	
	var newPicButtonListener = function(){
		sketchpad.clear();
		newPicButton.fadeOut(500);
		arrows.fadeOut(500);
	}
	newPicButton.off("click", newPicButtonListener);
	newPicButton.on("click", newPicButtonListener);
	
	var buttonListener = function(){
		arrows.fadeOut(0);
		var classname = $(this).attr("data-key");
		$("." + classname).fadeIn(500);
	}
	buttons.off("click", buttonListener);
	buttons.on("click", buttonListener);	
	
	startButtonListener = function(){
		audio[0].play();
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(10);
	}, 2000);
}

launch["frame-115"] = function() {
		theFrame = $("#frame-115"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		boyThinking = $(prefix + ".boy-thinking"),
		cloud = $(prefix + ".cloud");
	
	audio[0] = new Audio("audio/s14-1.mp3");
	
	audio[0].addEventListener("ended", function(){ 
		theFrame.attr("data-done", "true");
		fadeNavsInAuto();
		sendCompletedStatement(11);
	});
	
	fadeNavsOut();
	fadeLauncherIn();
	boyThinking.fadeOut(0);
	cloud.fadeOut(0);
	
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			boyThinking.fadeIn(500);
			cloud.fadeIn(500);
			audio[0].play();
		}, 1000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(11);
	}, 1000);
}

launch["frame-116"] = function() {
		theFrame = $("#frame-116"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		bg = $(prefix + ".bg-base"),
		zoomIn = $(prefix + ".zoom-in"),
		aiO = $(prefix + ".ai-o"),
		aiO2 = $(prefix + ".ai-o-2");
		
	audio[0] = new Audio("audio/s15-1.mp3");
	
	audio[0].addEventListener("ended", function(){
		theFrame.attr("data-done", "true");
		fadeNavsInAuto();
		sendCompletedStatement(12);
	});
	
	sprite[0] = new Motio(aiO[0], {
		"fps": "1",
		"frames": "7"
	});
	sprite[0].on("frame", function(){
		if(this.frame === this.frames - 1)
			this.pause();
	});
	
	sprite[1] = new Motio(aiO2[0], {
		"fps": "1",
		"frames": "6"
	});
	sprite[1].on("frame", function(){
		if(this.frame === this.frames - 1)
			this.pause();
	});
	
	zoomIn.fadeOut(0);
	aiO.fadeOut(0);
	aiO2.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	
	startButtonListener = function(){
		audio[0].play();
		timeout[0] = setTimeout(function(){
			zoomIn.fadeIn(1000);
		}, 3000);
		timeout[1] = setTimeout(function(){
			aiO.fadeIn(500);
		}, 19000);
		timeout[2] = setTimeout(function(){
			aiO.fadeOut(500);
			aiO2.fadeIn(500);
		}, 29000);
		timeout[3] = setTimeout(function(){
			sprite[1].to(1, true);
		}, 29500);
		timeout[4] = setTimeout(function(){
			sprite[1].to(2, true);
		}, 30500);
		timeout[5] = setTimeout(function(){
			sprite[1].play();
		}, 33000);
		timeout[6] = setTimeout(function(){
			aiO2.fadeOut(500);
			aiO.fadeIn(500);
		}, 41000);
		timeout[7] = setTimeout(function(){
			sprite[0].play();
		}, 51000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(12);
	}, 2000);
}

launch["frame-117"] = function() {
		theFrame = $("#frame-117"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		arrows = $(prefix + ".arrow"),
		baskets = $(prefix + ".basket"),
		upArrows = $(prefix + ".up-arrow"),
		downArrows = $(prefix + ".down-arrow"),
		elements = $(prefix + ".element"),
		keepThinking = $(prefix + ".keep-thinking"),
		result = $(prefix + ".result"),
		infos = $(prefix + ".info"),
		timer = $(prefix + ".timer"),
		currPair = 1; //currently active pair

	audio[0] = new Audio("audio/s16-1.mp3");
	
	audio[0].addEventListener("ended", function(){
		startTimer2(timer, 0);
	});
	
	keepThinking.fadeOut(0);
	result.fadeOut(0);
	elements.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	
	var arrowListener = function(){
		var classname = $(this).attr("data-info"),
			info = $("." + classname),
			num;
			
			if(info.html() === "")
				num = 1;
			else
				num = parseInt(info.html());
			
			inc = 0;
			
		$(this).hasClass("up-arrow") ? inc = 1 : inc = -1;
		
		num += inc;
		
		if(classname.indexOf("index") > -1)
		{
			if(num === 0 || num === 1)
				info.html("");
			else 
				info.html(num);
		}			
		else
		{
			num <= 0 ? info.html(num) : info.html("+" + num);
		}
		
		if(num === parseInt(info.attr("data-correct")))
			info.css("background-image", "url(pics/s17-grey.png)");
		else
			info.css("background-image", "");
		
		var allCorrect = true;
		for(var i = 0; i < infos.length; i++)
			if(!isCorrect($(infos[i])))
				allCorrect = false;
			
		if(allCorrect)
		{
			if(currPair < 10)
			{
				keepThinking.fadeIn(500);
				
				timeout[0] = setTimeout(function(){
					infos.filter(".charge").html(""+0);
					infos.filter(".index").html("");
					infos.css("background-image", "");
					
					baskets.css({
						"font-size": "",
						"text-align": ""
					});
					
					baskets.html("Drag the symbol of the element here");
					currPair++;
					keepThinking.fadeOut(500);
					setCorrectAnswers();
				}, 5000);
			}
			else
			{
				stopTimer2();
				presentPlane();
				timeout[0] = setTimeout(function(){
					result.fadeOut(0);
					theFrame.attr("data-done", "true");
					fadeNavsInAuto();
					sendCompletedStatement(13);
				}, 6000);
			}
		}
	}
	arrows.off("click", arrowListener);
	arrows.on("click", arrowListener);
	
	var setCorrectAnswers = function(){
		$(".charge-1").attr("data-correct", $(".pair-"+currPair+".element-1").attr("data-charge"));
		$(".index-1").attr("data-correct", $(".pair-"+currPair+".element-1").attr("data-index"));
		
		$(".charge-2").attr("data-correct", $(".pair-"+currPair+".element-2").attr("data-charge"));
		$(".index-2").attr("data-correct", $(".pair-"+currPair+".element-2").attr("data-index"));
		
		elements.fadeOut(0);
		$(".pair-" + currPair).fadeIn(0);
	}
	
	startButtonListener = function(){
		setCorrectAnswers();
		audio[0].play();
		timeout[4] = setTimeout(function(){
			blink2(elements, 1000, 2, function(){});
		}, 2000);		
		timeout[5] = setTimeout(function(){
			blink2(infos.filter(".charge"), 1000, 2, function(){});
		}, 12000);
		timeout[6] = setTimeout(function(){
			blink2(infos.filter(".index"), 1000, 2, function(){});
		}, 24000);
	};
		
	var successCondition = function(vegetable, basket){
		return basket === "basket";
	}
	
	var successFunction = function(vegetable, basket){
		basket.html(vegetable.attr("data-key"));
		basket.css({
			"font-size": "5vmax",
			"text-align": "center"
		});
		vegetable.css({
			"left": "",
			"top": "",
			"z-index": ""
		});
	}
	
	var failFunction = function(vegetable, basket){
		vegetable.css({
			"left": "",
			"top": "",
			"z-index": ""
		});
	}
	
	var finishCondition = function(){}
	
	var finishFunction = function(){}
	
	var dragTask = new DragTask(elements, successCondition, successFunction, failFunction, finishCondition, finishFunction);
	
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(13);
	}, 2000);
}

launch["frame-119"] = function() {
		theFrame = $("#frame-119"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		bg = $(prefix + ".bg-base"),
		boy1 = $(prefix + ".boy-1"), 
		boy2 = $(prefix + ".boy-2"), 
		bulb = $(prefix + ".bulb");
	
	var bulbSound = new Audio("audio/ding-sound.mp3");
	
	audio[0] = new Audio("audio/s18-1.mp3");
	
	sAudio = [];
	
	boy2.fadeOut(0);
	bulb.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	
	timeout[0] = setTimeout(function(){
		bulbSound.play();
		boy1.fadeOut(0);
		boy2.fadeIn(0);
		bulb.fadeIn(0);
	}, 1000);
	timeout[1] = setTimeout(function(){
		audio[0].play();
		timeout[4] = setTimeout(function(){
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
			sendCompletedStatement(15);
		}, 21000);
	}, 3000);
}

launch["frame-201"] = function() {
	audio[0] = new Audio("audio/s19-1.mp3");
	
	timeout[0] = setTimeout(function(){
		audio[0].play();
	}, 1000);
}

launch["frame-202"] = function() {
		theFrame = $("#frame-202"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		result = $(prefix + ".result"),
		turbine = $(prefix + ".turbine"),
		turbineButtons = $(prefix + ".turbine-button"),
		tasks = $(prefix + ".task"),
		currentTask = 0,
		answers = $(prefix + ".answer"), 
		awardLabel = $(prefix + ".award .label");
	
	audio[0] = new Audio("audio/turbine.mp3");
	
	audio[0].addEventListener("ended", function(){
		this.play();
	});
	
	var nextTask = function(){
		if(currentTask === 10)
		{
			audio[0].pause();
			result.html("Number of correct answers: " + awardLabel.html());
			result.fadeIn(500);
			timeout[7] = setTimeout(function(){
				result.fadeOut(0);
				theFrame.attr("data-done", "true");
				fadeNavsInAuto();
				sendCompletedStatement(22);
			}, 4000);
		}
		else
		{
			currTask = $(".task-" + currentTask);
			currTask.css("left", "150%");
			timeout[1] = setTimeout(function(){
				tasks.fadeOut(0);
				currTask.css("left", "");
				currTask = $(".task-" + (++currentTask));
				currTask.css("left", "-50%");
			}, 500);
			timeout[2] = setTimeout(function(){
				currTask.fadeIn(0);
				currTask.css("left", "");
			}, 1000);
			timeout[3] = setTimeout(function(){
				$(".task-"+currentTask+".elname").css("opacity", "1");
			}, 2000);
		}
	}
		
	var turbineButtonsListener = function(){
		nextTask();
	}
	turbineButtons.off("click", turbineButtonsListener);
	turbineButtons.on("click", turbineButtonsListener);
	
	var answerListener = function(){
		currAnswer = $(this);
		
		if(currAnswer.attr("data-correct"))
		{
			if(currAnswer.hasClass("answer-1"))
				currAnswer.css("background-image", "url(pics/test-cloud-1-green.png)");
			if(currAnswer.hasClass("answer-2"))
				currAnswer.css("background-image", "url(pics/test-cloud-2-green.png)");
			if(currAnswer.hasClass("answer-3"))
				currAnswer.css("background-image", "url(pics/test-cloud-3-green.png)");
			
			var planesNum = parseInt(awardLabel.html());
			awardLabel.html(++planesNum);
		}
		else
		{
			if(currAnswer.hasClass("answer-1"))
				currAnswer.css("background-image", "url(pics/test-cloud-1-red.png)");
			if(currAnswer.hasClass("answer-2"))
				currAnswer.css("background-image", "url(pics/test-cloud-2-red.png)");
			if(currAnswer.hasClass("answer-3"))
				currAnswer.css("background-image", "url(pics/test-cloud-3-red.png)");
			
			var correct,
				currAnswers = $(prefix + ".task-" + currentTask + ".answer")
			
			for(var i = 0; i < currAnswers.length; i++)
				if($(currAnswers[i]).attr("data-correct"))
					correct = $(currAnswers[i]);
				
			if(correct.hasClass("answer-1"))
				correct.css("background-image", "url(pics/test-cloud-1-green.png)");
			if(correct.hasClass("answer-2"))
				correct.css("background-image", "url(pics/test-cloud-2-green.png)");
			if(correct.hasClass("answer-3"))
				correct.css("background-image", "url(pics/test-cloud-3-green.png)");
		}
	}
	answers.off("click", answerListener);
	answers.on("click", answerListener);
	
	tasks.fadeOut(0);
	result.fadeOut(0);
	$(".task-" + currentTask).fadeIn(0);
	turbine.fadeOut(0);
	
	timeout[0] = setTimeout(function(){
		turbine.fadeIn(500);
		audio[0].play();
	}, 3000);
	timeout[1] = setTimeout(function(){
		audio[0].volume = 0.8;
	}, 4000);
	timeout[2] = setTimeout(function(){
		audio[0].volume = 0.6;
	}, 5000);
	timeout[3] = setTimeout(function(){
		audio[0].volume = 0.4;
	}, 6000);
	timeout[4] = setTimeout(function(){
		audio[0].volume = 0.2;
	}, 8000);
	timeout[5] = setTimeout(function(){
		audio[0].volume = 0.1;
	}, 9000);
	timeout[6] = setTimeout(function(){
		audio[0].volume = 0;
	}, 10000);

	startButtonListener = function(){
		
	}
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(22);
	}, 2000);
}

launch["frame-301"] = function() {
		theFrame = $("#frame-301"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		answer = $(prefix + ".answer"),
		questions = $(prefix + ".question"),
		checkButton = $(prefix + ".check-button"),
		backButton = $(prefix + ".back-button"),
		nextQuestionButton = $(prefix + ".next-question-button"),
		prevQuestionButton = $(prefix + ".prev-question-button"),
		activeQuestion = 1,
		balls = $(prefix + ".ball");
		
	var successCondition = function(vegetable, basket){
		return vegetable === basket;
	};
	
	var successFunction = function(vegetable, basket){
		basket.html(vegetable.html());
		vegetable.remove();		
	};
	
	var failFunction = function(vegetable, basket){
		vegetable.css("left", "");
		vegetable.css("top", "");
	};
	
	var finishCondition = function()
	{
		return $(prefix + ".ball").length === 0;
	}
	
	var finishFunction = function()
	{
		nextQuestionButton.fadeIn(500);
	}
	
	var dragTask = new DragTask(balls, successCondition, successFunction, failFunction, finishCondition, finishFunction);
	
	answer.addClass("answer-hidden");
	questions.fadeOut(0);
	checkButton.fadeOut(0);
	nextQuestionButton.fadeIn(0);
	prevQuestionButton.fadeOut(0);
	
	var nextListener = function(){
		if(activeQuestion < 6)
			activeQuestion ++;

		questions.fadeOut(0);
		
		answer.addClass("answer-hidden");
		nextQuestionButton.fadeIn(0);
		
		checkButton.html("Answer");
		
		if(activeQuestion === 2)
			checkButton.fadeOut(0);
		else
			checkButton.fadeIn(500);
		
		$(prefix + ".question-" + activeQuestion).fadeIn(500);
		
		if(activeQuestion === 1)
		{
			prevQuestionButton.fadeOut(0);
			nextQuestionButton.fadeIn(0);
		}
		else if(activeQuestion === 5)
		{
			answer.removeClass("answer-hidden");
			checkButton.fadeOut(0);
		}
		else if(activeQuestion === 6)
		{
			nextQuestionButton.fadeOut(0);
		}
		else
		{
			prevQuestionButton.fadeIn(0);
		}
	};
	nextQuestionButton.off("click", nextListener);
	nextQuestionButton.on("click", nextListener);
	
	var prevListener = function(){
		if(activeQuestion > 0)
			activeQuestion --;
		if(activeQuestion === 1 || activeQuestion === 2)
			checkButton.fadeOut(0);
		else
			checkButton.fadeIn(500);
		questions.fadeOut(0);
		
		answer.addClass("answer-hidden");
		checkButton.html("Показать ответ");
		
		nextQuestionButton.fadeIn(0);
		
		$(prefix + ".question-" + activeQuestion).fadeIn(500);
		
		if(activeQuestion === 1)
		{
			prevQuestionButton.fadeOut(0);
			nextQuestionButton.fadeIn(0);
		}
		else if(activeQuestion === 5)
		{
			answer.removeClass("answer-hidden");
			checkButton.fadeOut(0);
		}
		else
		{
			prevQuestionButton.fadeIn(0);
		}
	};
	prevQuestionButton.off("click", prevListener);
	prevQuestionButton.on("click", prevListener);
	
	var checkListener = function(){
		var currButton = $(this),
			currAnswer = $(prefix + ".answer-" + activeQuestion);
		
		if(activeQuestion !== 6)
			nextQuestionButton.fadeIn(500);
		
		currAnswer.toggleClass("answer-hidden");
		
		if(currButton.html() == "Скрыть ответ")
		{
			currButton.html("Показать ответ")
		}		
		else
		{
			currButton.html("Скрыть ответ");
		}
	};
	checkButton.off("click", checkListener);
	checkButton.on("click", checkListener);
	
	timeout[0] = setTimeout(function(){
		$(prefix + ".question-" + activeQuestion).fadeIn(0);
		answer.addClass("transition-2s");
	}, 1000);
}

var hideEverythingBut = function(elem)
{
	if(theFrame)
		theFrame.html(theClone.html());
		
	var frames = $(".frame");
	
		frames.fadeOut(0);
		elem.fadeIn(0);
		elemId = elem.attr("id"),
		regBox = $(".reg-box");
	
	fadeTimerOut();
	
	if(!globalName)
		regBox.fadeOut(0);
	
	if(elemId === "frame-000")
	{
		fadeNavsOut();
		fadeTimerOut();
	}
	
	for (var i = 0; i < audio.length; i++)
		audio[i].pause();		
	
	for (var i = 0; i < timeout.length; i++)
	{
		clearTimeout(timeout[i]);
		clearTimeout(timerTimeout);
	}
	
	for (var i = 0; i < sprite.length; i++)
		sprite[i].pause();
	
	for (var i = 0; i < typingText.length; i++)
		typingText[i].stopWriting();
	
	if(elem.attr("id") === "frame-000")
	{
		regBox.fadeIn(0);
		$(".award-box").fadeOut(0);
	}
	else if(elem.attr("id") === "frame-201" || elem.attr("id") === "frame-202")
	{
		$(".award-box").fadeOut(0);
	}
	else if(elem.attr("id") === "frame-301")
	{
		$(".award-box").fadeOut(0);
	}
	else
		$(".award-box").fadeIn(0);
	
	$(".plane-center").fadeOut(0);
	
	launch[elemId]();
	initMenuButtons();
}

var initMenuButtons = function(){
	var links = $(".link");
	links.click(function(){
		var elem = $("#"+$(this).attr("data-link"));
		hideEverythingBut(elem);
	});
};

var main = function()
{
	var video = $(".intro-video"),
		pic = $(".intro-pic");
	
	initMenuButtons();
	hideEverythingBut($("#frame-000"));
	
	setMenuStuff();
	video.attr("width", video.parent().css("width"));
	video.attr("height", video.parent().css("height"));
	video[0].play(); 
	
	timeout[0] = setTimeout(function(){
		video.hide();
	}, 10000);
	timeout[1] = setTimeout(function(){
		pic.hide(); 
    }, 15000);
};

$(document).ready(main);